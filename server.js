const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');

const users = require('./data/users.json');
const texts = require('./data/texts.json');

require('./passport.config');

server.listen(3000);

passport.serializeUser(function(user, done) {
    done(null, user);
  });
  
passport.deserializeUser(function(user, done) {
    done(null, user);
  });

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/game', (req, res) => {
    res.sendFile(path.join(__dirname, 'game.html'));
});

app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname, 'login.html'));
});

app.get('/text', passport.authenticate('jwt'), (req, res) => {
    const text = texts.find(text => text.id === +req.query.randomNum);
    res.status(200).json({ text });
});

app.post('/login', (req, res) => {
    const userFromReq = req.body;
    const userInDB = users.find(user => user.email === userFromReq.email);
    if (userInDB && userInDB.password === userFromReq.password) {
      const token = jwt.sign(userInDB, 'someSecret', { expiresIn: '24h' });
      res.status(200).json({ auth: true, token });
    } else {
      res.status(401).json({ auth: false });
    }
  });

let allUsers = [];

io.on('connection', socket => { 
    socket.on('addUser', payload => {
        const { token } = payload;
        const user = jwt.verify(token, 'someSecret');
        if (user) {
            const userLogin = jwt.decode(token).login;
            const isUnique = allUsers.find(el => el.userLogin === userLogin) ? false : true;
            if (isUnique) {
                allUsers.push({ userLogin, score: 0, winner: false });

                socket.broadcast.emit('newUser', { user: userLogin });
                socket.emit('newUser', { user: userLogin });        
            }
            if (allUsers.length === 1) {
                const randomFunc = (max, min) => Math.floor(Math.random() * (max - min + 1)) + min;
                const randomNum = randomFunc(texts.length - 1, 0);

                socket.broadcast.emit('startCountdown', { startCountdown: true, randomNum });
                socket.emit('startCountdown', { startCountdown: true, randomNum });
            }
        }  
    });

    socket.on('trackResults', payload => {
        const { token, text } = payload;
        const user = jwt.verify(token, 'someSecret');
        if (user) {
            const userLogin = jwt.decode(token).login;
            const score = text.filter(el => el === '\n').length;
            const userIndex = allUsers.findIndex(el => el.userLogin === userLogin);
            allUsers[userIndex].score = score;
            allUsers[userIndex].winner = score === text.length;

            socket.emit('showResults', allUsers);
            socket.broadcast.emit('showResults', allUsers);
        }
    });
});
