
window.onload = () => {

    const jwt = localStorage.getItem('jwt');
    if (!jwt) {
        location.replace('/login');
    } else {
        const socket = io.connect('http://localhost:3000');
        
        const usersList = document.querySelector('#users-list');
        const connectBtn = document.querySelector('#connect');
        const countDown = document.querySelector('#countdown');
        const inputFild = document.querySelector('#inputFild');
        const textWrapper = document.querySelector('#textWrapper')

        connectBtn.addEventListener('click', ev => {
            socket.emit('addUser', { token: jwt });
        });
        
    
        socket.on('newUser', payload => {
            const newLi = document.createElement('li');
            newLi.innerHTML = `${payload.user}`;
            usersList.appendChild(newLi);
        });

        socket.on('startCountdown', payload => {
            const { randomNum } = payload;
            const countDownDate = new Date();
            countDownDate.setSeconds(countDownDate.getSeconds() + 30);
            
            const timer = setInterval(() => {
                const now = new Date();
              
                let distance = countDownDate.getTime() - now.getTime();
                
                let seconds = Math.floor((distance % (1000 * 60)) / 1000);
                countDown.innerHTML = `Game will start in ${seconds > 0 ? seconds : 0}s`;
              
              if (distance < 0) {
                clearInterval(timer);
                inputFild.style.visibility = 'visible';
                inputFild.focus();
                
                fetch(`/text?randomNum=${randomNum}`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${jwt}`
                    }
                }).then(res => {
                    res.json().then(response => {
                        textWrapper.innerHTML = response.text.body;
                        let text = response.text.body.split('');
                        inputFild.addEventListener('keypress', ev => {
                            let textToBeUpdated = response.text.body.split('');
                            let currentElem = text.find(el => el !== '\n');
                            let currentIndex = text.indexOf(currentElem);                        
                            if (ev.keyCode === currentElem.charCodeAt(0)) {
                                textToBeUpdated[currentIndex] = `<strong class='success'>${text[currentIndex]}</strong>`;
                                text[currentIndex] = '\n';
                                socket.emit('trackResults', { token: jwt, text })
                            } else {
                                textToBeUpdated[currentIndex] = `<strong class='failure'>${text[currentIndex]}</strong>`;
                            }
                            textWrapper.innerHTML = textToBeUpdated.join('');
                        });


                    })
                }).catch(err => {
                    console.log('request went wrong');
                })
              }
            }, 100);
        });

        socket.on('showResults', payload => {
            usersList.innerHTML = '';
            payload.forEach(el => {
                let newLi = document.createElement('li');
                if (el.winner) {
                    newLi.classList.add('winner');
                    inputFild.disabled = true;
                    const countDownDate = new Date();
                    countDownDate.setSeconds(countDownDate.getSeconds() + 20);
                    const timer = setInterval(() => {
                        const now = new Date();
                        let distance = countDownDate.getTime() - now.getTime();
                        let seconds = Math.floor((distance % (1000 * 60)) / 1000);
                        document.querySelector('#countdown').innerHTML = `Room will be closed in ${seconds}s`;
                    
                    if (distance < 0) {
                        clearInterval(timer);
                        localStorage.removeItem('jwt');
                        location.replace('/game');
                    }
                    }, 100);
                }
                newLi.innerHTML = `${el.userLogin} - ${el.score}`;
                usersList.appendChild(newLi);
            });
        });

    }

}